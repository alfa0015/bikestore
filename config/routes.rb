Rails.application.routes.draw do
  devise_for :users,controllers: { sessions: "users/sessions" }
  authenticated :user do
    root to: 'welcome#admin', :as => "cpanel"
  end
  unauthenticated :user do
    root to: "welcome#index", :as => "unauthenticated"
  end
  namespace :api, :defaults => { :format => 'json' } do
    namespace :v1 do
      #match '*path', :controller => 'application', :action => 'xhr_options_request', :constraints => {:method => "OPTIONS"}
      get '/catalogos/marcas', to: 'catalogos#marcas'
      resources :users, except: [:new,:edit]
      resources :menus, except: [:new,:edit]
      resources :galeries, except: [:new,:edit]
      resources :contacts, except:[:new,:edit,:destroy]
      resources :pages, except:[:new,:edit]
      resources :catalogos, except:[:new,:edit]
      resources :news, except:[:new,:edit]
      devise_scope :user do
        post :sessions, :to => 'sessions#create'
        delete :session, :to => 'sessions#destroy'
        post :registrations, :to => 'registrations#create'
        delete :registration, :to => 'registration#destroy'
      end
    end
  end
end
