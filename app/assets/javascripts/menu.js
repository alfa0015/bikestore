$(function() {
	$(window).load(function(){	
		$(".button-collapse").sideNav({
			closeOnClick: true,
			'edge': 'left'
		});
		$('.collapsible').collapsible();
		$('select').material_select();
		$('.modal-trigger').leanModal();
	});
});