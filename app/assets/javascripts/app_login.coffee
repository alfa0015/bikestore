angular.module("login",["ngRoute","templates"])
.config(($routeProvider)->
		$routeProvider
			.when("/",{
					controller:"LoginCtrl"
					templateUrl:"login.html"
				})
			.otherwise({
					redirectTo:"/"
				})
	)
.controller("LoginCtrl",($scope,$http,$window)->
		$scope.submit = ()->
			email = $scope.email
			password = $scope.password
			$http.post("http://localhost:3000/api/v1/sessions",{user:{email:email,password:password}})
				.success((data,status)->
						console.log data
						$window.location.href = '/admin';
					)
				.error((error,status)->
						console.log error
						console.log status
					)
	)