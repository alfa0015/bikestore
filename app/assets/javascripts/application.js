// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require jquery.nivo.slider
//= require materialize
//= require menu
//= require angular
//= require angular-route
//= require angular-resource
//= require angular-animate
//= require angular-rails-templates
//= require angular-touch
//= require dirPagination
//= require satellizer.min
//= require angular-local-storage.min
//= require ng-file-upload-shim.min
//= require ng-file-upload.min
//= require app
//= require_tree ./templates
//= require controllers/controllers
//= require directives/directives
