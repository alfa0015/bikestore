json.array! @news do |new|
	json.(new,:id,:title,:cover,:path)
	json.cover_url new.cover.url
	json.cover_phone_url new.cover.url(:phone)
	json.cover_tablet_url new.cover.url(:tablet)
	json.cover_desktop_url new.cover.url(:desktop)
end
