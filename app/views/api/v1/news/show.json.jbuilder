json.(@new,:id,:title,:path,:cover_file_name,:cover_content_type,:cover_file_size)
json.cover_url @new.cover.url
json.cover_phone_url @new.cover.url(:phone)
json.cover_tablet_url @new.cover.url(:tablet)
json.cover_desktop_url @new.cover.url(:desktop)
