json.(@catalogo,:id,:marca,:modelo,:tipo,:color,:material,:sizes,:precio,:imagen_file_name,:imagen_content_type,:imagen_file_size)
json.imagen_url @catalogo.imagen.url
json.imagen_phone_url @catalogo.imagen.url(:phone)
json.imagen_tablet_url @catalogo.imagen.url(:tablet)
json.imagen_desktop_url @catalogo.imagen.url(:desktop)
