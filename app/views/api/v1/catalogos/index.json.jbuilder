json.array! @catalogos do |catalogo|
	json.(catalogo, :id, :marca, :modelo,:tipo)
	json.imagen_url catalogo.imagen.url
	json.imagen_phone_url catalogo.imagen.url(:phone)
	json.imagen_tablet_url catalogo.imagen.url(:tablet)
	json.imagen_desktop_url catalogo.imagen.url(:desktop)
end