json.(@galery,:id,:image_file_name,:image_content_type,:image_file_size)
json.image_url @galery.image.url
json.image_phone_url @galery.image.url(:phone)
json.image_tablet_url @galery.image.url(:tablet)
json.image_desktop_url @galery.image.url(:desktop)
