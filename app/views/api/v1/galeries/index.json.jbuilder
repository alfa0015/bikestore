json.array! @galeries do |galery|
	json.id galery.id
	json.image_url galery.image.url
	json.image_phone_url galery.image.url(:phone)
	json.image_tablet_url galery.image.url(:tablet)
	json.image_desktop_url galery.image.url(:desktop)
end