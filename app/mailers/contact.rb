class Contact < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.contact.notify.subject
  #

  default from: "noreplay@domain.com"

  def notify(email)
    @greeting = "Hi"

    mail(to: "alfa0015.rafael@gmail.com",subject:"Contacto De #{email}")
  end
end
