# == Schema Information
#
# Table name: menus
#
#  id         :integer          not null, primary key
#  name       :string
#  attr_id    :string
#  attr_class :string
#  path       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Menu < ActiveRecord::Base

	validates :name, presence: true, length:{minimum: 3}
	validates :path, presence: true, length:{minimum: 3}
	
end