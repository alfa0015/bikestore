# == Schema Information
#
# Table name: galeries
#
#  id                 :integer          not null, primary key
#  image_file_name    :string
#  image_content_type :string
#  image_file_size    :integer
#  image_updated_at   :datetime
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#

require 'fileutils'

class Galery < ActiveRecord::Base



	validates :image, attachment_presence: true
	validates_with AttachmentSizeValidator, attributes: :image, less_than: 10.megabytes

	#has_attached_file :avatar, styles: { phone: "320 x 568 >",table: " 800 x 1280 >", desktop: "1280 x 72>" }, default_url: "/images/galery/:style/"
	has_attached_file :image,styles: { phone: "320 x 568 >",table: " 1280 x 800 >", desktop: "1280 x 720>" }
	validates_attachment_content_type :image, content_type: /\Aimage\/.*\Z/
	validates_attachment_file_name :image, matches: [/png\Z/, /jpe?g\Z/,/gif\Z/]

	def self.destroy_file_attchment(id)
		system("rm -rf #{Rails.root}/public/system/galeries/images/000/000/#{id}")
	end
end
