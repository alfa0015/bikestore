# == Schema Information
#
# Table name: tokens
#
#  id         :integer          not null, primary key
#  expires_at :datetime
#  user_id    :integer
#  token      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Token < ActiveRecord::Base
  belongs_to :user

  def is_valid?
  	DateTime.now < self.expires_at
  end

  private
  	def self.generate_token
  		begin
  			token = SecureRandom.hex
  		end while Token.where(token: token).any?
  		expires_at ||= 1.month.from_now
      Token.create(:expires_at=>expires_at,:token=>token)
  	end

end
