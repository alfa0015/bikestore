# == Schema Information
#
# Table name: news
#
#  id                 :integer          not null, primary key
#  title              :string
#  cover_file_name    :string
#  cover_content_type :string
#  cover_file_size    :integer
#  cover_updated_at   :datetime
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  path               :string
#

class New < ActiveRecord::Base
  #Validaciones
  validates :cover, attachment_presence: true
  validates_with AttachmentSizeValidator, attributes: :cover, less_than: 10.megabytes

  #Agregar soporte para subida de archivos
  has_attached_file :cover,styles: { phone: "320 x 568 >",table: " 1280 x 800 >", desktop: "1280 x 720>" },:preserve_files => "false"
  validates_attachment_content_type :cover, content_type: /\Aimage\/.*\Z/
  validates_attachment_file_name :cover, matches: [/png\Z/,/jpe\Z/, /jpe?g\Z/,/gif\Z/]
end
