# == Schema Information
#
# Table name: my_contacts
#
#  id         :integer          not null, primary key
#  email      :string
#  subject    :string
#  body       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  state      :string           default("unread")
#

class MyContact < ActiveRecord::Base

	include AASM

	after_create :send_email

	validates_presence_of :email,:body,:subject
	validates_format_of :email , with: Devise::email_regexp
	validates :subject, presence: true, length:{minimum: 10}
	validates :body, presence: true, length:{minimum: 20}

	aasm column: "state" do
		state :unread, initial: true
		state :read

		event :unread do
			transitions from: :read, to: :unread
		end

		event :read do
			transitions from: :unread, to: :read
		end
	end

	def send_email
		Contact.notify(self).deliver_now
	end

end
