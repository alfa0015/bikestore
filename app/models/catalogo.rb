# == Schema Information
#
# Table name: catalogos
#
#  id                  :integer          not null, primary key
#  marca               :string
#  modelo              :string
#  imagen_file_name    :string
#  imagen_content_type :string
#  imagen_file_size    :integer
#  imagen_updated_at   :datetime
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  tipo                :string
#  color               :string
#  material            :string
#  sizes               :string
#  precio              :decimal(10, 2)
#

class Catalogo < ActiveRecord::Base
	validates :imagen, attachment_presence: true
	validates_with AttachmentSizeValidator, attributes: :imagen, less_than: 10.megabytes

	#has_attached_file :avatar, styles: { phone: "320 x 568 >",table: " 800 x 1280 >", desktop: "1280 x 72>" }, default_url: "/images/galery/:style/"
	has_attached_file :imagen,styles: { phone: "320 x 568 >",tablet: " 1280 x 800 >", desktop: "1280 x 720>" },processors: [:thumbnail, :compression]
	validates_attachment_content_type :imagen, content_type: /\Aimage\/.*\Z/
	validates_attachment_file_name :imagen, matches: [/png\Z/,/jpe\Z/, /jpe?g\Z/,/gif\Z/]
end
