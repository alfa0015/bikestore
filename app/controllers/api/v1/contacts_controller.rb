class Api::V1::ContactsController < Api::V1::ApplicationController
	before_action :set_contact, only:[:show,:update]
	#before_action :authenticate, except:[:create]
	def index
		@contacts = MyContact.all
	end

	def show
	end

	def create
		@contact = MyContact.new(contacts_params)
		if @contact.save
			render json:{messages:"Su Correo se a enviado"}, status: :ok
		else
			render json:{ errors: @contact.full_messages}, status: :unprocessable_entity
		end
	end
	
	def update
		if contacts_params[:state]=="read"
			if @contact.may_read?
				if @contact.read!
					render "/api/v1/contacts/show"
				else
					render json:{errors:@contact.errors},status: :unprocessable_entity
				end
			else
				render json:{errors:"read cannot transition from read"},status: :unprocessable_entity 
			end
		elsif contacts_params[:state]=="unread"
			if @contact.may_unread?
				if @contact.unread!
					render "/api/v1/contacts/show"
				else
					render json:{errors:@contact.errors},status: :unprocessable_entity
				end
			else
				render json:{errors:"unread cannot transition from unread"},status: :unprocessable_entity 
			end
		end
	end

	private

		def set_contact
			@contact = MyContact.find(params[:id]) if params[:id]
		end

		def contacts_params
			params.require(:contact).permit(:email,:subject,:body,:state)
		end
end