class Api::V1::ApplicationController < ApplicationController
	# Prevent CSRF attacks by raising an exception.
	# For APIs, you may want to use :null_session instead.
	protect_from_forgery with: :null_session
	#after_action :cors_set_access_control_headers

	# def cors_set_access_control_headers
	# 	headers['Access-Control-Allow-Origin']='*'
	# 	headers['Access-Control-Allow-Methods']='POST,GET,PUT,DELETE,OPTIONS'
	# 	headers['Access-Control-Allow-Headers']='Origin,Content-Type,Accept,Authorization,Token'

	# end
	def xhr_options_request
		
	end
	def authenticate
		token_str = params[:token]
		token = Token.find_by(token: token_str)

		if token.nil? || !token.is_valid?
			render json:{errors:"Tu token es invalido"},status: :unauthorized
		else
			current_user = token.user
		end
	end

end