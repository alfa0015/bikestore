class Api::V1::GaleriesController < Api::V1::ApplicationController
	before_action :authenticate, except:[:index,:show]
	before_action :set_galery, except:[:index]
	
	def index
		@galeries = Galery.order(:id)
	end

	def show

	end

	def create
		@galery = Galery.new(galery_params)
		if @galery.save
			render "/api/v1/galeries/show"
		else
			render json:{ errors: @galery.errors.full_messages}, status: :unprocessable_entity
		end
	end

	def update
		if @galery.update(galery_params)
			render '/api/v1/galeries/show',status: :ok
		else
			render json:{errors:@galery.errors.full_messages},status: :unprocessable_entity
		end
	end

	def destroy
		Galery.destroy_file_attchment(@galery.id)
		if @galery.destroy
			render json:{messages:"La imagen fue eliminada"},status: :ok
		else
			render json:{errors:@galery.errors},status: :unprocessable_entity
		end
	end

	private

		def set_galery
			@galery = Galery.find(params[:id]) if params[:id]
		end

		def galery_params
			params.require(:galeries).permit(:image)
		end
end