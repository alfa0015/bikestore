class Api::V1::NewsController < Api::V1::ApplicationController
	before_action :authenticate, except:[:index,:show]
	before_action :set_new, except:[:index,:create]
	def index
		@news = New.order(:id).all
	end
	def show
	end
	def create
		@new = New.new(new_params)
		if @new.save
			render '/api/v1/news/show',status: :ok
		else
			render json:{errors:@new.errors.full_messages},status: :unprocessable_entity
		end
	end
	def update
		if @new.update(new_params)
			render '/api/v1/news/show',status: :ok
		else
			render json:{errors:@new.errors.full_messages},status: :unprocessable_entity
		end
	end
	def destroy
		if @new.destroy
			render json:{messages:"El registro fue eliminado"},status: :ok
		else
			render json:{errors:@new.errors.full_messages},status: :unprocessable_entity
		end
	end

	private
		def set_new
			@new = New.find(params[:id])
		end
		def new_params
			params.require(:new).permit(:title,:cover,:path)
		end

end
