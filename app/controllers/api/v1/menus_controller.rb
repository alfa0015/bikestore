class Api::V1::MenusController < Api::V1::ApplicationController
	before_action :authenticate, except:[:index,:show]
	before_action :set_menu, only:[:show,:update,:destroy]

	def index
		@menus = Menu.order(:id).all
	end

	def show
	end

	def create
		@menu = Menu.new(menus_params)
		if @menu.save
			render "/api/v1/menus/show"
		else
			render json:{ errors: @menu.errors.full_messages}, status: :unprocessable_entity 
		end
	end

	def update
		if @menu.update(menus_params)
			render "/api/v1/menus/show"
		else
			render json:{errors:@menu.errors},status: :unprocessable_entity
		end
	end

	def destroy
		if @menu.destroy 
			render json:{messages:"El menu fue eliminado"},status: :ok
		else
			render json:{errors:@menu.errors},status: :unprocessable_entity
		end
	end

	private

		def menus_params
			params.require(:menu).permit(:name,:attr_id,:attr_class,:path)
		end

		def set_menu
			@menu = Menu.find(params[:id])
		end
end