class Api::V1::CatalogosController < Api::V1::ApplicationController
	before_action :authenticate, except:[:index,:show]
	before_action :set_catalogo, except:[:index,:create,:marcas]

	def index
		@catalogos = Catalogo.order(:id).all
	end
	def show
	end
	def create
		@catalogo = Catalogo.new(catalogo_params)
		if @catalogo.save
			render '/api/v1/catalogos/show',status: :ok
		else
			render json:{errors:@catalogo.errors.full_messages},status: :unprocessable_entity
		end
	end
	def update
		if @catalogo.update(catalogo_params)
			render '/api/v1/catalogos/show',status: :ok
		else
			render json:{errors:@catalogo.errors.full_messages},status: :unprocessable_entity
		end
	end
	def destroy
		if @catalogo.destroy
			render json:{messages:"El registro fue eliminado"},status: :ok
		else
			render json:{errors:@catalogo.errors.full_messages},status: :unprocessable_entity
		end
	end
	def marcas
		@marcas = Catalogo.select(:marca).uniq
	end
	private
		def set_catalogo
			@catalogo = Catalogo.find(params[:id])
		end
		def catalogo_params
			params.require(:catalogo).permit(:marca,:modelo,:imagen,:tipo)
		end
end
