class Api::V1::PagesController < Api::V1::ApplicationController
	before_action :set_page,except:[:index]
	#before_action :authenticate,except:[:show]
	def index
		@pages = Page.all
	end

	def show		
	end

	def create
		@page = Page.new(pages_params)
		if @page.save
			render "/api/v1/pages/show"
		else
			render json:{errors:@page.errors.full_messages},status: :unprocessable_entity
		end
	end

	def update
		if @page.update(pages_params)
			render "/api/v1/pages/show"
		else
			render json:{errors:@page.errors.full_messages},status: :unprocessable_entity
		end
	end

	def destroy
		if @page.destroy
			render json:{messages:"La pagina fue eliminada"},status: :ok
		else
			render json:{errors:@page.errors.full_messages},status: :unprocessable_entity
		end
	end

	private
		def set_page
			@page = Page.find(params[:id]) if params[:id]
		end

		def pages_params
			params.require(:page).permit(:title,:body)
		end
end