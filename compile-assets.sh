rm -rf public/assets
bundle exec rake assets:clobber RAILS_ENV=production
bundle exec rake assets:clean RAILS_ENV=production
bundle exec rake assets:precompile RAILS_ENV=production
chmod -R 777 public/assets