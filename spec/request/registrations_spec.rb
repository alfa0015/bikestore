require 'rails_helper'

RSpec.describe Api::V1::RegistrationsController, type: :request do
	context "datos validos" do 
		describe "POST /registrations" do
			before :each do 
				user = {email:"test@local.local",password:"12345678",password_confirmation:"12345678"}
				post "/api/v1/registrations",{ user: user}
			end
			it{	expect(response).to have_http_status(:ok) }

			it{ change(User,:count).by(1)}

			it "responds with user found or create" do 
				json = JSON.parse(response.body)
				expect(json["email"]).to eq("test@local.local")
			end
		end
	end

	context "datos invalidos" do 
		describe "POST /registrations" do
			before :each do 
				user = {email:"test@local.local",password:"12",password_confirmation:"12"}
				post "/api/v1/registrations",{ user: user}
			end
			it{	expect(response).to have_http_status(:unprocessable_entity) }

			it{ change(User,:count).by(0)}

			it "responds with error" do 
				json = JSON.parse(response.body)
				expect(json["errors"]).to_not be_empty
			end
		end
	end
end