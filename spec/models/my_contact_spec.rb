# == Schema Information
#
# Table name: my_contacts
#
#  id         :integer          not null, primary key
#  email      :string
#  subject    :string
#  body       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  state      :string           default("unread")
#

require 'rails_helper'

RSpec.describe MyContact, type: :model do

  it{ should validate_presence_of(:email) }
  it{ should validate_presence_of(:subject) }
  it{ should validate_presence_of(:body) }

  it{should_not allow_value("root@mail").for(:email)}
  it{should allow_value("root@mail.com").for(:email)}

  it{should validate_length_of(:subject).is_at_least(10)}
  it{should validate_length_of(:body).is_at_least(20)}

  it "Deveria mandar un correo" do
  	expect{
  		FactoryGirl.create(:my_contact)
  	}.to change(ActionMailer::Base.deliveries,:count).by(1)
  end
end
