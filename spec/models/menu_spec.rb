# == Schema Information
#
# Table name: menus
#
#  id         :integer          not null, primary key
#  name       :string
#  attr_id    :string
#  attr_class :string
#  path       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

RSpec.describe Menu, type: :model do
	#Test for field Name
	it{should validate_presence_of(:name)}
	it{should_not allow_value("aa").for(:name)}
	it{should validate_length_of(:name).is_at_least(4)}
	#Test for field path
	it{should validate_presence_of(:path)}
	it{should_not allow_value("aa").for(:path)}
	it{should validate_length_of(:path).is_at_least(10)}
end
