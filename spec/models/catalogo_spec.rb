# == Schema Information
#
# Table name: catalogos
#
#  id                  :integer          not null, primary key
#  marca               :string
#  modelo              :string
#  imagen_file_name    :string
#  imagen_content_type :string
#  imagen_file_size    :integer
#  imagen_updated_at   :datetime
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  tipo                :string
#  color               :string
#  material            :string
#  sizes               :string
#  precio              :decimal(10, 2)
#

require 'rails_helper'

RSpec.describe Catalogo, type: :model do
  pending "add some examples to (or delete) #{__FILE__}"
end
