# == Schema Information
#
# Table name: tokens
#
#  id         :integer          not null, primary key
#  expires_at :datetime
#  user_id    :integer
#  token      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

RSpec.describe Token, type: :model do

  it "Deberia retornar valido cuando si no a expirado" do
  	token = FactoryGirl.create(:token,expires_at: DateTime.now + 1.minute)
  	expect(token.is_valid?).to eq(true)
  end

  it "Deberia retornar invalido cuando si esta a expirado" do
  	token = FactoryGirl.create(:token,expires_at: DateTime.now - 1.minute)
  	expect(token.is_valid?).to eq(false)
  end

end
