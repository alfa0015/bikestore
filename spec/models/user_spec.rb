# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :inet
#  last_sign_in_ip        :inet
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  provider               :string
#  uid                    :string
#

require 'rails_helper'

RSpec.describe User, type: :model do
  it{should validate_presence_of(:email)}
  it{should validate_uniqueness_of(:email).case_insensitive}
  it{should validate_presence_of(:password)}


  it "Deberia crear el usuario si el email no existe" do
  	expect{
  		User.from_sign_in({email:"root@gmail.com",password:"12345678",password_confirmation:"12345678"})
  		}.to change(User,:count).by(1)
  end

  it "Deberia de encontrar el usuario si el email y el password son validos" do
  	user = FactoryGirl.create(:user)
  	expect{
  		User.from_sign_in({email:user.email,password:user.password})
  		}.to change(User,:count).by(0)
  end
end
