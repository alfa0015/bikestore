# == Schema Information
#
# Table name: galeries
#
#  id                 :integer          not null, primary key
#  image_file_name    :string
#  image_content_type :string
#  image_file_size    :integer
#  image_updated_at   :datetime
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#

FactoryGirl.define do
  factory :galery do
    image Rack::Test::UploadedFile.new("#{Rails.root}/spec/images/test.jpg", "image/jpg")
  end
end
