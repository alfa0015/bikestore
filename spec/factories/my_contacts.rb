# == Schema Information
#
# Table name: my_contacts
#
#  id         :integer          not null, primary key
#  email      :string
#  subject    :string
#  body       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  state      :string           default("unread")
#

FactoryGirl.define do
	factory :my_contact do
		email "root@mail.com"
		subject "Test de correo"
		body "Test de correo para verificar el compartamiento de la app"
	end
end
