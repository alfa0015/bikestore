# == Schema Information
#
# Table name: news
#
#  id                 :integer          not null, primary key
#  title              :string
#  cover_file_name    :string
#  cover_content_type :string
#  cover_file_size    :integer
#  cover_updated_at   :datetime
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  path               :string
#

FactoryGirl.define do
  factory :news do
    title "MyString"
    cover ""
  end
end
