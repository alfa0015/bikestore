# == Schema Information
#
# Table name: tokens
#
#  id         :integer          not null, primary key
#  expires_at :datetime
#  user_id    :integer
#  token      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

FactoryGirl.define do
  factory :token do
    expires_at "2016-03-17 14:10:06"
	association :user, factory: :user
  end
end
