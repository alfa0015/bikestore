# == Schema Information
#
# Table name: menus
#
#  id         :integer          not null, primary key
#  name       :string
#  attr_id    :string
#  attr_class :string
#  path       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

FactoryGirl.define do
  factory :menu do
    name "MyString"
    attr_id "MyString"
    attr_class "MyString"
    path "http://google.com"
  end
end
