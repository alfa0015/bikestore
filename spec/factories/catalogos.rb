# == Schema Information
#
# Table name: catalogos
#
#  id                  :integer          not null, primary key
#  marca               :string
#  modelo              :string
#  imagen_file_name    :string
#  imagen_content_type :string
#  imagen_file_size    :integer
#  imagen_updated_at   :datetime
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  tipo                :string
#  color               :string
#  material            :string
#  sizes               :string
#  precio              :decimal(10, 2)
#

FactoryGirl.define do
  factory :catalogo do
    marca "MyString"
    modelo "MyString"
    imagen ""
  end
end
