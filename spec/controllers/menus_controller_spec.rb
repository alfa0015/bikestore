# == Schema Information
#
# Table name: menus
#
#  id         :integer          not null, primary key
#  name       :string
#  attr_id    :string
#  attr_class :string
#  path       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

RSpec.describe Api::V1::MenusController, type: :request do
	describe "GET /menus" do
		before :each do 
			FactoryGirl.create_list(:menu,10)
			get api_v1_menus_path
		end

		it { expect(response).to have_http_status(:ok)}

		it "mande la lista de menus" do
			json = JSON.parse(response.body)
			expect(json.length).to eq(Menu.count)
		end
	end

	describe "GET /menus/:id" do
		
		before :each do 
			@menu = FactoryGirl.create(:menu)
			get  api_v1_menu_path(@menu.id)
		end

		it { expect(response).to have_http_status(:ok) }
			
		it "manda el menu solicitada" do
			json = JSON.parse(response.body)
			expect(json["id"]).to eq(@menu.id)
		end

		it "Manda los atributos del menu" do
			json = JSON.parse(response.body)
			expect(json.keys).to contain_exactly("id","name","attr_id","attr_class")
		end
	end

	describe "POST /menus" do

		context "Con Token Valido" do

			before :each do 
				user = FactoryGirl.build(:user)
				usuario = {email:user.email,password:user.password,password_confirmation:user.password_confirmation}
				post '/api/v1/registrations',{user: usuario}
				@token = JSON.parse(response.body)
				@menu = FactoryGirl.build(:menu)
				post api_v1_menus_path,{token: @token["token"],menu:{name:"home",path:@menu.path}}
			end

			it { expect(response).to have_http_status(:ok) }

			it "Crea un menu" do
				expect{
					post api_v1_menus_path,{token: @token["token"],menu:{name:"home",path:"http://google.com"} }
				}.to change(Menu,:count).by(1)
			end

			it "Responde con el menu creado" do
				json = JSON.parse(response.body)
				expect(json["name"]).to eq("home")
			end
		end

		context "Con Token Invalido" do

			before :each do 
				post api_v1_menus_path
			end

			it { expect(response).to have_http_status(:unauthorized) }

			it "responde con errores al guardar el menu" do
				json = JSON.parse(response.body)
				expect(json["errors"]).to_not be_empty
			end

		end

		context "Parametros Invalidos" do

			before :each do 
				user = FactoryGirl.build(:user)
				usuario = {email:user.email,password:user.password,password_confirmation:user.password_confirmation}
				post '/api/v1/registrations',{user: usuario}
				@token = JSON.parse(response.body)
				@menu = FactoryGirl.build(:menu)
				post api_v1_menus_path,{token: @token["token"],menu:{name:@menu.name}}
			end

			it { expect(response).to have_http_status(:unprocessable_entity) }

			it "responde con errores al guardar el menu" do
				json = JSON.parse(response.body)
				expect(json["errors"]).to_not be_empty
			end

		end
	end

	describe "PATCH/PUT /menus/:id" do

		context "Con token valido" do
			
			before :each do
				user = FactoryGirl.build(:user)
				usuario = {email:user.email,password:user.password,password_confirmation:user.password_confirmation}
				post '/api/v1/registrations',{user: usuario}
				@token = JSON.parse(response.body)
				@menu = FactoryGirl.create(:menu)
				patch api_v1_menu_path(@menu.id),{token:@token["token"],menu:{name:"Nuevo name",path:@menu.path}}
			end

			it { expect(response).to have_http_status(:ok) }

			it "Actualiza la encuesta indicada" do
				json = JSON.parse(response.body)
				expect(json["name"]).to eq("Nuevo name")
			end

		end

		context "Con token Invalido" do

			before :each do
				user = FactoryGirl.build(:user)
				usuario = {email:user.email,password:user.password,password_confirmation:user.password_confirmation}
				post '/api/v1/registrations',{user: usuario}
				@token = JSON.parse(response.body)
				@menu = FactoryGirl.create(:menu)
				patch api_v1_menu_path(@menu.id),{menu:{name:"Nuevo name",path:@menu.path}}
			end

			it { expect(response).to have_http_status(:unauthorized) }

			it "Debe responder con el erros" do 
				json = JSON.parse(response.body)
				expect(json["errors"]).to_not be_empty
			end
		end

	end

	describe "DELETE /menus/:id" do

		context "Con Token valido" do 
			before :each do
				user = FactoryGirl.build(:user)
				usuario = {email:user.email,password:user.password,password_confirmation:user.password_confirmation}
				post '/api/v1/registrations',{user: usuario}
				@token = JSON.parse(response.body)
				@menu = FactoryGirl.create(:menu)
				
			end

			it { 
				delete api_v1_menu_path(@menu.id),{token:@token["token"]}
				expect(response).to have_http_status(:ok) 
			}

			it "Elimina el menu indicado" do 
				expect{
					delete api_v1_menu_path(@menu.id),{token:@token["token"]}
					}.to change(Menu,:count).by(-1)
			end
		end

		context "Con Token Invalido" do
			before :each do
				user = FactoryGirl.build(:user)
				usuario = {email:user.email,password:user.password,password_confirmation:user.password_confirmation}
				post '/api/v1/registrations',{user: usuario}
				@token = JSON.parse(response.body)
				@menu = FactoryGirl.create(:menu)
				delete api_v1_menu_path(@menu.id)
			end

			it { expect(response).to have_http_status(:unauthorized) }

			it "responde con errores al guardar el menu" do
				json = JSON.parse(response.body)
				expect(json["errors"]).to_not be_empty
			end
		end
	end
end
