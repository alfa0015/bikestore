# == Schema Information
#
# Table name: pages
#
#  id         :integer          not null, primary key
#  title      :string
#  body       :string
#  url        :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

RSpec.describe Api::V1::PagesController, type: :request do
	describe "GET /pages" do
		context "Con Token Valido" do
			before :each do 
				user = FactoryGirl.build(:user)
				usuario = {email:user.email,password:user.password,password_confirmation:user.password_confirmation}
				post '/api/v1/registrations',{user: usuario}
				@token = JSON.parse(response.body)
				FactoryGirl.create_list(:page,10)
				get api_v1_pages_path,{token: @token["token"]}
			end
			it { expect(response).to have_http_status(:ok)}
			it "mande la lista de pages" do
				json = JSON.parse(response.body)
				expect(json.length).to eq(Page.count)
			end
		end
		context "Con Token Invalido" do
			before :each do
				get api_v1_pages_path
			end
			it { expect(response).to have_http_status(:unauthorized) }
			it "responde con errores al guardar el menu" do
				json = JSON.parse(response.body)
				expect(json["errors"]).to_not be_empty
			end
		end
	end
	describe "GET /pages/:id" do
		before :each do 
			@page = FactoryGirl.create(:page)
			get api_v1_page_path(@page.id)
		end
		it { expect(response).to have_http_status(:ok) }
			
		it "manda el menu solicitada" do
			json = JSON.parse(response.body)
			expect(json["id"]).to eq(@page.id)
		end

		it "Manda los atributos del menu" do
			json = JSON.parse(response.body)
			expect(json.keys).to contain_exactly("id","title","body","url","created_at")
		end
	end
	describe "POST /pages" do
		context "Con Token Valido" do
			before :each do 
				user = FactoryGirl.build(:user)
				usuario = {email:user.email,password:user.password,password_confirmation:user.password_confirmation}
				post '/api/v1/registrations',{user: usuario}
				@token = JSON.parse(response.body)
				@page = FactoryGirl.build(:page)
				post api_v1_pages_path,{token: @token["token"],page:{title:@page.title,body:@page.body}}
			end
			it { expect(response).to have_http_status(:ok) }
			it "Responde con el menu creado" do
				json = JSON.parse(response.body)
				expect(json["title"]).to eq("MyString MyString MyString")
			end
		end
		context "Con Token Valido y Etiquetas HTML Validas" do
			before :each do 
				user = FactoryGirl.build(:user)
				usuario = {email:user.email,password:user.password,password_confirmation:user.password_confirmation}
				post '/api/v1/registrations',{user: usuario}
				@token = JSON.parse(response.body)
				@page = FactoryGirl.build(:page)
			end
			it "Deveria dejar guardar la etiqueta a" do
				post api_v1_pages_path,{token: @token["token"],page:{title:@page.title,body:"<a href='google.com'>Hola</a>"}}
				json = JSON.parse(response.body)
				expect(json["body"]).to eq("<a href='google.com'>Hola</a>")
			end
			it "Deveria dejar guardar la etiqueta ol li" do
				post api_v1_pages_path,{token: @token["token"],page:{title:@page.title,body:"<ol><li>1</li></ol>"}}
				json = JSON.parse(response.body)
				expect(json["body"]).to eq("<ol><li>1</li></ol>")
			end
			it "Deveria dejar guardar la etiqueta ul li" do
				post api_v1_pages_path,{token: @token["token"],page:{title:@page.title,body:"<ul><li>1</li></ul>"}}
				json = JSON.parse(response.body)
				expect(json["body"]).to eq("<ul><li>1</li></ul>")
			end
			it "Deveria dejar guardar la etiqueta b" do
				post api_v1_pages_path,{token: @token["token"],page:{title:@page.title,body:"<b>Hola</b>"}}
				json = JSON.parse(response.body)
				expect(json["body"]).to eq("<b>Hola</b>")
			end
			it "Deveria dejar guardar la etiqueta i" do
				post api_v1_pages_path,{token: @token["token"],page:{title:@page.title,body:"<i>Hola</i>"}}
				json = JSON.parse(response.body)
				expect(json["body"]).to eq("<i>Hola</i>")
			end
			it "Deveria dejar guardar la etiqueta div" do
				post api_v1_pages_path,{token: @token["token"],page:{title:@page.title,body:"<div>Hola</div>"}}
				json = JSON.parse(response.body)
				expect(json["body"]).to eq("<div>Hola</div>")
			end
		end
		context "Con Token Valido y Etiquetas HTML no Validas" do
			before :each do 
				user = FactoryGirl.build(:user)
				usuario = {email:user.email,password:user.password,password_confirmation:user.password_confirmation}
				post '/api/v1/registrations',{user: usuario}
				@token = JSON.parse(response.body)
				@page = FactoryGirl.build(:page)
			end
			it "Deveria limpiar la etiqueta script" do
				post api_v1_pages_path,{token: @token["token"],page:{title:@page.title,body:"<script>alert('hola');</script>"}}
				json = JSON.parse(response.body)
				expect(json["body"]).to eq("alert('hola');")
			end
			it "Deveria limpiar la etiqueta script" do
				post api_v1_pages_path,{token: @token["token"],page:{title:@page.title,body:"<script src='js/script.js'></script>"}}
				json = JSON.parse(response.body)
				expect(json["body"]).to eq("")
			end
		end
		context "Con Token Invalido" do
			before :each do
				@page = FactoryGirl.build(:page)
				post api_v1_pages_path,{page:{title:@page.title,body:@page.body}}
			end
			it { expect(response).to have_http_status(:unauthorized) }
			it "responde con errores al guardar el menu" do
				json = JSON.parse(response.body)
				expect(json["errors"]).to_not be_empty
			end
		end
	end
	describe "PUT/PATCH /pages/:id" do
		context "Con Token Valido" do
			before :each do 
				user = FactoryGirl.build(:user)
				usuario = {email:user.email,password:user.password,password_confirmation:user.password_confirmation}
				post '/api/v1/registrations',{user: usuario}
				@token = JSON.parse(response.body)
				@page = FactoryGirl.create(:page)
				put api_v1_page_path(@page.id),{token: @token["token"],page:{title:"Nuevo Titulo de la Pagina",body:@page.body}}
			end
			it { expect(response).to have_http_status(:ok) }
			it "Actualiza la encuesta indicada" do
				json = JSON.parse(response.body)
				expect(json["title"]).to eq("Nuevo Titulo de la Pagina")
			end
		end
		context "Con Token Invalido" do
			before :each do 
				@page = FactoryGirl.create(:page)
				put api_v1_page_path(@page.id),{page:{title:"Nuevo Titulo de la Pagina",body:@page.body}}
			end
			it { expect(response).to have_http_status(:unauthorized) }
			it "Debe responder con el erros" do 
				json = JSON.parse(response.body)
				expect(json["errors"]).to_not be_empty
			end
		end
	end
	describe "DELETE /pages/:id" do
		context "Con Token Valido" do
			before :each do 
				user = FactoryGirl.build(:user)
				usuario = {email:user.email,password:user.password,password_confirmation:user.password_confirmation}
				post '/api/v1/registrations',{user: usuario}
				@token = JSON.parse(response.body)
				@page = FactoryGirl.create(:page)
			end
			it { 
				delete api_v1_page_path(@page.id),{token: @token["token"]}
				expect(response).to have_http_status(:ok) 
			}
			it "Elimina La pagina" do 
				expect{
					delete api_v1_page_path(@page.id),{token: @token["token"]}
					}.to change(Page,:count).by(-1)
			end
		end
		context "Con Token Invalido" do
			before :each do 
				@page = FactoryGirl.create(:page)
				delete api_v1_page_path(@page.id)
			end
			it { expect(response).to have_http_status(:unauthorized) }

			it "responde con errores al guardar el menu" do
				json = JSON.parse(response.body)
				expect(json["errors"]).to_not be_empty
			end
		end
	end
end
