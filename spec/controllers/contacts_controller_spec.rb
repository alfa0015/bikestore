require 'rails_helper'
RSpec.describe Api::V1::ContactsController, type: :request do
	describe "GET /contacts" do
		context "Con Token Valido" do
			before :each do 
				user = FactoryGirl.build(:user)
				usuario = {email:user.email,password:user.password,password_confirmation:user.password_confirmation}
				post '/api/v1/registrations',{user: usuario}
				@token = JSON.parse(response.body)
				FactoryGirl.create_list(:my_contact,10)
				get api_v1_contacts_path,{token: @token["token"]}
			end
			it { expect(response).to have_http_status(:ok)}

			it "mande la lista de los mensajes" do
				json = JSON.parse(response.body)
				expect(json.length).to eq(MyContact.count)
			end
		end

		describe "Con Token Invalido" do
			before :each do 
				FactoryGirl.create_list(:my_contact,10)
				get api_v1_contacts_path
			end
			it { expect(response).to have_http_status(:unauthorized) }
			it "responde con errores al intentar acceder a los contacts" do
				json = JSON.parse(response.body)
				expect(json["errors"]).to_not be_empty
			end
		end
	end
	describe "GET /contacts/:id" do

		context "Con token Valido" do
			before :each do 
				user = FactoryGirl.build(:user)
				usuario = {email:user.email,password:user.password,password_confirmation:user.password_confirmation}
				post '/api/v1/registrations',{user: usuario}
				@token = JSON.parse(response.body)
				@contact = FactoryGirl.create(:my_contact)
				get api_v1_contact_path(@contact.id),{token:@token["token"]}
			end

			it { expect(response).to have_http_status(:ok) }

			it "manda el menu solicitada" do
				json = JSON.parse(response.body)
				expect(json["id"]).to eq(@contact.id)
			end

			it "Manda los atributos del contact" do
				json = JSON.parse(response.body)
				expect(json.keys).to contain_exactly("id","subject","email","body","state","created_at","at_created")
			end
		end

		context "Con Token Invalido" do
			before :each do 
				@contact = FactoryGirl.create(:my_contact)
				get api_v1_contact_path(@contact.id)
			end
			it { expect(response).to have_http_status(:unauthorized) }
			it "responde con errores al intentar acceder a los contacts" do
				json = JSON.parse(response.body)
				expect(json["errors"]).to_not be_empty
			end
		end
	end
	describe "PATCH/PUT /contacts" do
		context "Con Token Valido" do
			before :each do
				user = FactoryGirl.build(:user)
				usuario = {email:user.email,password:user.password,password_confirmation:user.password_confirmation}
				post '/api/v1/registrations',{user: usuario}
				@token = JSON.parse(response.body)
			end

			it { expect(response).to have_http_status(:ok) }
			it "Actualiza el estado a read" do
				@contact = FactoryGirl.create(:my_contact)
				patch api_v1_contact_path(@contact.id),{token:@token["token"],contact:{state:"read"}}
				json = JSON.parse(response.body)
				expect(json["state"]).to eq("read")
			end
			it "Actualiza el estado a unread" do
				@contact = FactoryGirl.create(:my_contact,state:"read")
				patch api_v1_contact_path(@contact.id),{token:@token["token"],contact:{state:"unread"}}
				json = JSON.parse(response.body)
				expect(json["state"]).to eq("unread")
			end
			it"Debe de mandar error al tratar de actualizar unread a unread"do
				@contact = FactoryGirl.create(:my_contact)
				patch api_v1_contact_path(@contact.id),{token:@token["token"],contact:{state:"unread"}}
				json = JSON.parse(response.body)
				expect(json["errors"]).to_not be_empty
			end
			it"Debe de mandar error al tratar de actualizar read a read"do
				@contact = FactoryGirl.create(:my_contact,state:"read")
				patch api_v1_contact_path(@contact.id),{token:@token["token"],contact:{state:"read"}}
				json = JSON.parse(response.body)
				expect(json["errors"]).to_not be_empty
			end
		end
		context "Con token Invalido" do
			before :each do 
				@contact = FactoryGirl.create(:my_contact)
				patch api_v1_contact_path(@contact.id),{contact:{state:"read"}}
			end
			it { expect(response).to have_http_status(:unauthorized) }
			it "responde con errores al guardar el menu" do
				json = JSON.parse(response.body)
				expect(json["errors"]).to_not be_empty
			end
		end
	end
	describe "POST /contacts" do
		before :each do 
			@contact = FactoryGirl.build(:my_contact)
			post api_v1_contacts_path,{contact:{email:@contact.email,subject:@contact.subject,body:@contact.body}}
		end
		it { expect(response).to have_http_status(:ok) }
		it "Crea un Contact" do
			expect{
				post api_v1_contacts_path,{contact:{email:@contact.email,subject:@contact.subject,body:@contact.body}}
			}.to change(MyContact,:count).by(1)
		end

		it "Deveria mandar un correo" do
			expect{
		  		post api_v1_contacts_path,{contact:{email:@contact.email,subject:@contact.subject,body:@contact.body}}
		  	}.to change(ActionMailer::Base.deliveries,:count).by(1)
		end

		it "Responde Su Correo se a enviado al crear el contact" do
			json = JSON.parse(response.body)
			expect(json["messages"]).to eq("Su Correo se a enviado")
		end
	end
end