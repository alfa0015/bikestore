# == Schema Information
#
# Table name: galeries
#
#  id                 :integer          not null, primary key
#  image_file_name    :string
#  image_content_type :string
#  image_file_size    :integer
#  image_updated_at   :datetime
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#

require 'rails_helper'

RSpec.describe Api::V1::GaleriesController, type: :request do

	describe "GET /galeries" do
		context "Con Token Valido" do
			before :each do 
				user = FactoryGirl.build(:user)
				usuario = {email:user.email,password:user.password,password_confirmation:user.password_confirmation}
				post '/api/v1/registrations',{user: usuario}
				@token = JSON.parse(response.body)
				@image = FactoryGirl.create_list(:galery,2)
				get api_v1_galeries_path,{token: @token["token"]}
			end

			it { expect(response).to have_http_status(:ok)}

			it "Deveria mandar la lista de imagenes de la galeria" do 
				json = JSON.parse(response.body)
				expect(json.length).to eq(Galery.count)
			end

		end

		context "Con Token Invalido" do
			before :each do 
				get api_v1_galeries_path
			end

			it { expect(response).to have_http_status(:unauthorized)}

			it "responde con errores al guardar el menu" do
				json = JSON.parse(response.body)
				expect(json["errors"]).to_not be_empty
			end
		end
	end

	describe "GET /galeries/:id" do

		context "Token Valido" do 
			before :each do
				user = FactoryGirl.build(:user)
				usuario = {email:user.email,password:user.password,password_confirmation:user.password_confirmation}
				post '/api/v1/registrations',{user: usuario}
				@token = JSON.parse(response.body)
				@image = FactoryGirl.create(:galery)
				get api_v1_galery_path(@image.id),{token: @token["token"]}
			end

			it "Deveria regresar el id de la imagen solicitada" do
				json = JSON.parse(response.body)
				expect(json["id"]).to eq(@image.id)
			end

			it "Deveria de regresar la url de la imagen solicitada" do
				json = JSON.parse(response.body)
				expect(json["image_url"]).to_not be_empty
			end
		end

		context "Token Invalido" do
			before :each do 
				@image = FactoryGirl.create(:galery)
				get api_v1_galery_path(@image.id)
			end

			it { expect(response).to have_http_status(:unauthorized)}

			it "responde con errores al guardar el menu" do
				json = JSON.parse(response.body)
				expect(json["errors"]).to_not be_empty
			end
		end
	end

	describe "POST /galeries" do

		context "Con Token Valido"do
			before :each do 
				user = FactoryGirl.build(:user)
				usuario = {email:user.email,password:user.password,password_confirmation:user.password_confirmation}
				post '/api/v1/registrations',{user: usuario}
				@token = JSON.parse(response.body)
				post api_v1_galeries_path,{token: @token["token"],galeries: {image:Rack::Test::UploadedFile.new("#{Rails.root}/spec/images/test.jpg", "image/jpg")} };
			end

			it { expect(response).to have_http_status(:ok) }

			it "Crea una imagen" do
				expect{
					post api_v1_galeries_path,{token: @token["token"],galeries: {image:Rack::Test::UploadedFile.new("#{Rails.root}/spec/images/test.jpg", "image/jpg")} }
				}.to change(Galery,:count).by(1)
			end

			it "Responde con la imagen creada" do
				json = JSON.parse(response.body)
				expect(json["image_url"]).to_not be_empty
			end

			it "Manda los atributos del menu" do
				json = JSON.parse(response.body)
				expect(json.keys).to contain_exactly("id", "image_content_type", "image_file_name", "image_file_size","image_url","image_phone_url","image_tablet_url","image_desktop_url")
			end

		end

		context "Con Token Invalido" do
			before :each do 
				post api_v1_galeries_path,{galeries: {image:Rack::Test::UploadedFile.new("#{Rails.root}/spec/images/test.jpg", "image/jpg")} };
			end

			it { expect(response).to have_http_status(:unauthorized)}

			it "responde con errores al guardar el menu" do
				json = JSON.parse(response.body)
				expect(json["errors"]).to_not be_empty
			end
		end
	end

	describe "DELETE /galeries/:id" do
		
		context "Con Token Valido" do
			before :each do
				user = FactoryGirl.build(:user)
				usuario = {email:user.email,password:user.password,password_confirmation:user.password_confirmation}
				post '/api/v1/registrations',{user: usuario}
				@token = JSON.parse(response.body)
				@image = FactoryGirl.create(:galery)
			end

			it { 
				delete api_v1_galery_path(@image.id),{token:@token["token"]}
				expect(response).to have_http_status(:ok) 
			}

			it "Elimina el menu indicado" do 
				expect{
					delete api_v1_galery_path(@image.id),{token:@token["token"]}
					}.to change(Galery,:count).by(-1)
			end

		end

		context "Con Token Invalido" do
			before :each do
				user = FactoryGirl.build(:user)
				usuario = {email:user.email,password:user.password,password_confirmation:user.password_confirmation}
				post '/api/v1/registrations',{user: usuario}
				@token = JSON.parse(response.body)
				@image = FactoryGirl.create(:galery)
				delete api_v1_galery_path(@image.id)
			end

			it { expect(response).to have_http_status(:unauthorized) }

			it "responde con errores al guardar el menu" do
				json = JSON.parse(response.body)
				expect(json["errors"]).to_not be_empty
			end
		end
	end
end
