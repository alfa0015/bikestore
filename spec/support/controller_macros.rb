module ControllerMacros
  def login_user
    before(:each) do
      @request.env["devise.mapping"] = Devise.mappings[:user]
      user = FactoryGirl.create(:user)
      sign_in user
    end
  end
  def login_entidade
    before(:each) do
      @request.env["devise.mapping"] = Devise.mappings[:entidade]
      user = FactoryGirl.create(:entidade)
      sign_in entidade
    end
  end
end