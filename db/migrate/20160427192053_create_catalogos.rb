class CreateCatalogos < ActiveRecord::Migration
  def change
    create_table :catalogos do |t|
      t.string :marca
      t.string :modelo
      t.attachment :imagen

      t.timestamps null: false
    end
  end
end
