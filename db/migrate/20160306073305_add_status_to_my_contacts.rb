class AddStatusToMyContacts < ActiveRecord::Migration
  def up
  	add_column :my_contacts, :state, :string, default: "unread"
  end
  def down
  	remove_column :my_contacts, :state
  end
end
