class CreateGaleries < ActiveRecord::Migration
  def up
    create_table :galeries do |t|
      t.attachment :image

      t.timestamps null: false
    end
  end

  def down
  	drop_table :galeries
  end
end
