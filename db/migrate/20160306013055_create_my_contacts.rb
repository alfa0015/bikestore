class CreateMyContacts < ActiveRecord::Migration
  def up
    create_table :my_contacts do |t|
      t.string :email
      t.string :subject
      t.string :body

      t.timestamps null: false
    end
  end

  def down
  	drop_table :my_contacts
  end
end
