class CreateMenus < ActiveRecord::Migration
  def up
    create_table :menus do |t|
      t.string :name
      t.string :attr_id
      t.string :attr_class
      t.string :path

      t.timestamps null: false
    end
  end

  def down
  	drop_table :menus
  end
end
