class CreateNews < ActiveRecord::Migration
  def change
    create_table :news do |t|
      t.string :title
      t.attachment :cover

      t.timestamps null: false
    end
  end
end
