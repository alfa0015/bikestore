class AddToFieldToCatalogos < ActiveRecord::Migration
  def change
    add_column :catalogos, :color, :string
    add_column :catalogos, :material, :string
    add_column :catalogos, :sizes, :string
    add_column :catalogos, :precio, :decimal,precision: 10, scale: 2
  end
end
