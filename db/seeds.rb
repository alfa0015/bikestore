# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
menus = Menu.create([
    {
      name:"Ruta",
      path:"ruta"
    },
    {
      name:"MTB",
      path:"mtb"
    },
    {
      name:"Triatlón",
      path:"tri"
    },
    {
      name:"Partners",
      path:"partners"
    }
])
noticias = New.create([
    {
      title:"Nueva Gan Disk T600",
      path:"http://www.pinarello.com/en/bike-2016/road/gan-disk",
      cover:File.open(Rails.root.join('db/data/noticias/pinarello/noticia1.jpg'), 'rb')
    },
    {
      title:"Estas Listo para ser el centro de atención?",
      path:"http://www.pinarello.com/en/bike-2016/road/dogma-f8w",
      cover:File.open(Rails.root.join('db/data/noticias/pinarello/noticia2.jpg'), 'rb')
    },
    {
      title:"Ruta Negra Italia Patrocinada Por Bianchi",
      path:"http://www.bianchi.com/global/focuson/thomson-bike-tours-powered-by-bianchi-312897",
      cover:File.open(Rails.root.join('db/data/noticias/bianchi/noticia1.jpg'), 'rb')
    },
    {
      title:"Specialissima",
      path:"https://www.youtube.com/watch?v=ncKhIevLfSI",
      cover:File.open(Rails.root.join('db/data/noticias/bianchi/noticia2.jpg'), 'rb')
    },
    {
      title:"Aida Turcios",
      path:"#/bikestore",
      cover:File.open(Rails.root.join('db/data/noticias/bikestore/aida.jpg'), 'rb')
    },
    {
      title:"Claudia Villanova",
      path:"#/bikestore",
      cover:File.open(Rails.root.join('db/data/noticias/bikestore/atle2.jpg'), 'rb')
    },
    {
      title:"La Primera Dogma F8 Del País",
      path:"#/bikestore",
      cover:File.open(Rails.root.join('db/data/noticias/bikestore/dogma1.jpg'), 'rb')
    },
    {
      title:"Noticias de F8",
      path:"#/bikestore",
      cover:File.open(Rails.root.join('db/data/noticias/bikestore/dogma2.jpg'), 'rb')
    },
])
catalogos = Catalogo.create([
  {
    marca:"Bianchi",
    modelo:"Methanol 29.6 SX Evo XT 2X10",
    imagen:File.open(Rails.root.join('db/data/bianchi/Mtb/1 Bianchi - Methanol 29.6 SX Evo XT 2X10.jpg'), 'rb'),
    tipo:"mtb",
    color:"6J-Acid Green, 1H-Celeste",
    material:"Fibra de carbono",
    sizes:"17, 19, 21",
    precio:""
  },
  {
    marca:"Bianchi",
    modelo:"Jab 29.3 SLX-DEORE 3X10",
    imagen:File.open(Rails.root.join('db/data/bianchi/Mtb/2 Bianchi - Jab 29.3 SLX-DEORE 3X10.jpg'), 'rb'),
    tipo:"mtb",
    color:"T4-Celeste",
    material:"Aluminio",
    sizes:"17,19, 21",
    precio:""
  },
  {
    marca:"Bianchi",
    modelo:"Kuma 29.1 DEORE-ALIVIO 3X9",
    imagen:File.open(Rails.root.join('db/data/bianchi/Mtb/3 Bianchi - Kuma 29.1 DEORE-ALIVIO 3X9.jpg'), 'rb'),
    tipo:"mtb",
    color:"1Z-Black, 2Y-Titanio",
    material:"Aluminio",
    sizes:"15,17,19,21",
    precio:""
  },
  {
    marca:"Bianchi",
    modelo:"KUMA 29.2 ACERA-ALTUS 3X9",
    imagen:File.open(Rails.root.join('db/data/bianchi/Mtb/4 Bianchi - KUMA 29.2 ACERA-ALTUS 3X9.jpg'), 'rb'),
    tipo:"mtb",
    color:"1Z-Black, 2Y-Titanio",
    material:"Aluminio",
    sizes:"15,17,19,21",
    precio:""
  },
  {
    marca:"Bianchi",
    modelo:"KUMA 27.2 ACERA-ALTUS 3X9 DISC",
    imagen:File.open(Rails.root.join('db/data/bianchi/Mtb/5 Bianchi - KUMA 27.2 ACERA-ALTUS 3X9 DISC.jpg'), 'rb'),
    tipo:"mtb",
    color:"1Z-Black",
    material:"Aluminio",
    sizes:"17, 19, 21",
    precio:""
  },
  {
    marca:"Bianchi",
    modelo:"KUMA 27.3 ACERA-ALTUS 3X8 DISC",
    imagen:File.open(Rails.root.join('db/data/bianchi/Mtb/6 Bianchi - KUMA 27.3 ACERA-ALTUS 3X8 DISC.jpg'), 'rb'),
    tipo:"mtb",
    color:"1Z-Black",
    material:"Aluminio",
    sizes:"15, 17, 19, 21",
    precio:""
  },
  {
    marca:"Bianchi",
    modelo:"KUMA 27.4 ACERA-ALTUS 3X8 VBRAKE",
    imagen:File.open(Rails.root.join('db/data/bianchi/Mtb/7 Bianchi - KUMA 27.4 ACERA-ALTUS 3X8 VBRAKE.jpg'), 'rb'),
    tipo:"mtb",
    color:"T4-Celeste,",
    material:"Aluminio",
    sizes:"15, 17, 19, 21",
    precio:""
  },
  {
    marca:"Bianchi",
    modelo:"Specialissima Super Record EPS",
    imagen:File.open(Rails.root.join('db/data/bianchi/ruta/1 Bianchi - Specialissima Super Record EPS.jpg'), 'rb'),
    tipo:"ruta",
    color:"1H-Celeste Fluo Matte",
    material:"Fibra de carbono",
    sizes:"50-53-55-57-59-61",
    precio:""
  },
  {
    marca:"Bianchi",
    modelo:"Specialissima Dura Ace Di2",
    imagen:File.open(Rails.root.join('db/data/bianchi/ruta/2 Bianchi - Specialissima Dura Ace Di2.jpg'), 'rb'),
    tipo:"ruta",
    color:"1E-Black Matte",
    material:"Fibra de carbonor",
    sizes:"50-53-55-57-59-61",
    precio:""
  },
  {
    marca:"Bianchi",
    modelo:"Oltre XR.2 Super Record EPS",
    imagen:File.open(Rails.root.join('db/data/bianchi/ruta/3 Bianchi - Oltre XR.2 Super Record EPS.jpg'), 'rb'),
    tipo:"ruta",
    color:"B1-Black/Celeste",
    material:"Oltre XR.2 carbon",
    sizes:"50-53-55-57-59-61",
    precio:""
  },
  {
    marca:"Bianchi",
    modelo:"Oltre XR.2 Dura Ace Di2",
    imagen:File.open(Rails.root.join('db/data/bianchi/ruta/4 Bianchi - Oltre XR.2 Dura Ace Di2.jpg'), 'rb'),
    tipo:"ruta",
    color:"1E-Black matte",
    material:"Oltre XR.2 carbon",
    sizes:"50-53-55-57-59-61",
    precio:""
  },
  {
    marca:"Bianchi",
    modelo:"Oltre XR.1 Ultegra Di2",
    imagen:File.open(Rails.root.join('db/data/bianchi/ruta/5 Bianchi - Oltre XR.1 Ultegra Di2.jpg'), 'rb'),
    tipo:"ruta",
    color:"1R-Black Matte/Celeste",
    material:"Oltre XR.1 Carbon",
    sizes:"50-53-55-57-59-61",
    precio:""
  },
  {
    marca:"Bianchi",
    modelo:"Oltre XR.1 Ultegra",
    imagen:File.open(Rails.root.join('db/data/bianchi/ruta/6 Bianchi - Oltre XR.1 Ultegra.jpg'), 'rb'),
    tipo:"ruta",
    color:"KK-Black Matte",
    material:"Oltre XR.1 Carbon",
    sizes:"50-53-55-57-59-61",
    precio:""
  },
  {
    marca:"Bianchi",
    modelo:"Oltre XR.1 105",
    imagen:File.open(Rails.root.join('db/data/bianchi/ruta/7 Bianchi - Oltre XR.1 105.jpg'), 'rb'),
    tipo:"ruta",
    color:"Z2 - Black/Yellow fluo",
    material:"Oltre XR.1 Carbon",
    sizes:"50-53-55-57-59-61",
    precio:""
  },
  {
    marca:"Bianchi",
    modelo:"Infinito CV Chorus",
    imagen:File.open(Rails.root.join('db/data/bianchi/ruta/8 Bianchi - Infinito CV Chorus.jpg'), 'rb'),
    tipo:"ruta",
    color:"1Z-Black Matte/Celeste",
    material:"Infinito Countervail carbon",
    sizes:"50-53-55-57-59-61",
    precio:""
  },
  {
    marca:"Bianchi",
    modelo:"Infinito CV Disc Ultegra Di2",
    imagen:File.open(Rails.root.join('db/data/bianchi/ruta/9 Bianchi - Infinito CV Disc Ultegra Di2.jpg'), 'rb'),
    tipo:"ruta",
    color:"RK-Black Matte/Red",
    material:"Infinito Countervail carbon",
    sizes:"50-53-55-57-59-61",
    precio:""
  },
  {
    marca:"Bianchi",
    modelo:"Infinito CV Ultegra Di2",
    imagen:File.open(Rails.root.join('db/data/bianchi/ruta/10 Bianchi - Infinito CV Ultegra Di2.jpg'), 'rb'),
    tipo:"ruta",
    color:"7W-Black/Celeste",
    material:"Infinito Countervail carbon",
    sizes:"50-53-55-57-59-61",
    precio:""
  },
  {
    marca:"Bianchi",
    modelo:"Infinito CV Ultegra",
    imagen:File.open(Rails.root.join('db/data/bianchi/ruta/11 Bianchi - Infinito CV Ultegra.jpg'), 'rb'),
    tipo:"ruta",
    color:"7W-Black/Celeste",
    material:"Infinito Countervail carbon",
    sizes:"50-53-55-57-59-61",
    precio:""
  },
  {
    marca:"Bianchi",
    modelo:"Intenso Ultegra",
    imagen:File.open(Rails.root.join('db/data/bianchi/ruta/12 Bianchi - Intenso Ultegra.jpg'), 'rb'),
    tipo:"ruta",
    color:"1J-Celeste",
    material:"INTENSO - Fibra de carbono",
    sizes:"50-53-55-57-59-61",
    precio:""
  },
  {
    marca:"Bianchi",
    modelo:"Intenso Disc 105",
    imagen:File.open(Rails.root.join('db/data/bianchi/ruta/13 Bianchi - Intenso Disc 105.jpg'), 'rb'),
    tipo:"ruta",
    color:"1P-Black Matte",
    material:"INTENSO - Fibra de carbono",
    sizes:"50-53-55-57-59-61",
    precio:""
  },
  {
    marca:"Bianchi",
    modelo:"Intenso 105",
    imagen:File.open(Rails.root.join('db/data/bianchi/ruta/14 Bianchi - Intenso 105.jpg'), 'rb'),
    tipo:"ruta",
    color:"D6-White/Gray",
    material:"INTENSO - Fibra de carbono",
    sizes:"50-53-55-57-59-61",
    precio:""
  },
  {
    marca:"Bianchi",
    modelo:"Inteso Dura ACE Mix",
    imagen:File.open(Rails.root.join('db/data/bianchi/ruta/15 Bianchi - INTENSO DURA ACE MIX.jpg'), 'rb'),
    tipo:"ruta",
    color:"KK-Black",
    material:"INTENSO - Fibra de carbono",
    sizes:"50-53-55-57-59-61",
    precio:""
  },
  {
    marca:"Bianchi",
    modelo:"Intenso Veloce",
    imagen:File.open(Rails.root.join('db/data/bianchi/ruta/16 BIanchi - INTENSO VELOCE.jpg'), 'rb'),
    tipo:"ruta",
    color:"CK-Celeste",
    material:"INTENSO - Fibra de carbono",
    sizes:"50-53-55-57-59-61",
    precio:""
  },
  {
    marca:"Bianchi",
    modelo:"Impulso Disc 105",
    imagen:File.open(Rails.root.join('db/data/bianchi/ruta/17 Bianchi - Impulso Disc 105.jpg'), 'rb'),
    tipo:"ruta",
    color:"2Z-Black Matte",
    material:"Triple-Hydroformed aluminum",
    sizes:"50-53-55-57-59-61",
    precio:""
  },
  {
    marca:"Bianchi",
    modelo:"Impulso 105",
    imagen:File.open(Rails.root.join('db/data/bianchi/ruta/18 Bianchi - Impulso 105.jpg'), 'rb'),
    tipo:"ruta",
    color:"1J-Celeste",
    material:"Triple-Hydroformed aluminum",
    sizes:"50-53-55-57-59-61",
    precio:""
  },
  {
    marca:"Bianchi",
    modelo:"Impulso Tiagra",
    imagen:File.open(Rails.root.join('db/data/bianchi/ruta/19 Bianchi - Impulso Tiagra.jpg'), 'rb'),
    tipo:"ruta",
    color:"1J-Celeste",
    material:"Triple-Hydroformed aluminum",
    sizes:"50-53-55-57-59-61",
    precio:""
  },
  {
    marca:"Bianchi",
    modelo:"Via Nirone Sora",
    imagen:File.open(Rails.root.join('db/data/bianchi/ruta/20 Bianchi - Via Nirone Sora.jpg'), 'rb'),
    tipo:"ruta",
    color:"1P-Black/Celeste",
    material:"hydroformed aluminum",
    sizes:"46-50-53-55-57-59-61-63",
    precio:""
  },
  {
    marca:"Bianchi",
    modelo:"Via Nirone S-sport sora",
    imagen:File.open(Rails.root.join('db/data/bianchi/ruta/21 Bianchi - Via Nirone S-sport sora.jpg'), 'rb'),
    tipo:"ruta",
    color:"3M-Titanio",
    material:"hydroformed aluminum",
    sizes:"46-50-53-55-57-59-61-63",
    precio:""
  },
  {
    marca:"Bianchi",
    modelo:"Via Nirone Claris",
    imagen:File.open(Rails.root.join('db/data/bianchi/ruta/22 Bianchi - Via Nirone Claris.jpg'), 'rb'),
    tipo:"ruta",
    color:"7R-Black/Red",
    material:"hydroformed aluminum",
    sizes:"46-50-53-55-57-59-61-63",
    precio:""
  },
  {
    marca:"Bianchi",
    modelo:"Infinito CV Dama Ultegra",
    imagen:File.open(Rails.root.join('db/data/bianchi/ruta/23 Bianchi - Infinito CV Dama Ultegra.jpg'), 'rb'),
    tipo:"ruta",
    color:"7M-Celeste Matte",
    material:"Fibra de carbono",
    sizes:"47-50-53-55",
    precio:""
  },
  {
    marca:"Bianchi",
    modelo:"Intenso Dama Ultegra Di2",
    imagen:File.open(Rails.root.join('db/data/bianchi/ruta/24 Bianchi - Intenso Dama Ultegra Di2.jpg'), 'rb'),
    tipo:"ruta",
    color:"3E-Graphite/Celeste",
    material:"Intenso carbon",
    sizes:"47-50-53-55",
    precio:""
  },
  {
    marca:"Bianchi",
    modelo:"Intenso Dama Ultegra",
    imagen:File.open(Rails.root.join('db/data/bianchi/ruta/25 Bianchi - Intenso Dama Ultegra.jpg'), 'rb'),
    tipo:"ruta",
    color:"7Q-Celeste",
    material:"Intenso carbon",
    sizes:"47-50-53-55",
    precio:""
  },
  {
    marca:"Bianchi",
    modelo:"Intenso Dama 105",
    imagen:File.open(Rails.root.join('db/data/bianchi/ruta/26 Bianchi - Intenso Dama 105.jpg'), 'rb'),
    tipo:"ruta",
    color:"3J-Ice White/Celeste",
    material:"INTENSO - Fibra de carbono",
    sizes:"47-50-53-55",
    precio:""
  },
  {
    marca:"Bianchi",
    modelo:"Impulso Dama 105",
    imagen:File.open(Rails.root.join('db/data/bianchi/ruta/27 Bianchi - Impulso Dama 105.jpg'), 'rb'),
    tipo:"ruta",
    color:"7Q-Celeste",
    material:"Triple-Hydroformed aluminum",
    sizes:"47-50-53-55",
    precio:""
  },
  {
    marca:"Bianchi",
    modelo:"Impulso Dama Tiagra",
    imagen:File.open(Rails.root.join('db/data/bianchi/ruta/28 Bianchi - Impulso Dama Tiagra.jpg'), 'rb'),
    tipo:"ruta",
    color:"6Y-Azzurro, 3E-Graphite/Celeste",
    material:"Triple-Hydroformed aluminum",
    sizes:"47-50-53-55",
    precio:""
  },
  {
    marca:"Bianchi",
    modelo:"Via Nirone Dama Sora",
    imagen:File.open(Rails.root.join('db/data/bianchi/ruta/29 Bianchi - Via Nirone Dama Sora.jpg'), 'rb'),
    tipo:"ruta",
    color:"3J-White Metallic",
    material:"Triple-Hydroformed aluminum",
    sizes:"47-50-53-55",
    precio:""
  },
  {
    marca:"Bianchi",
    modelo:"Aquila CV Ultegra",
    imagen:File.open(Rails.root.join('db/data/bianchi/Tri/Bianchi - Aquila CV Ultegra.jpg'), 'rb'),
    tipo:"triatlon",
    color:"KW-Black",
    material:"Aquila Time Trial Countervail carbon",
    sizes:"XS-SM-MD-LG",
    precio:""
  },
  {
    marca:"Bianchi",
    modelo:"Aquila CV Dura Ace Di2",
    imagen:File.open(Rails.root.join('db/data/bianchi/Tri/Bianchi - Aquila CV Dura Ace Di2.jpg'), 'rb'),
    tipo:"triatlon",
    color:"KW-Black",
    material:"Aquila Time Trial Countervail carbon",
    sizes:"XS-SM-MD-LG",
    precio:""
  },
  {
    marca:"Pinarello",
    modelo:"Dogma XC 9.9",
    imagen:File.open(Rails.root.join('db/data/pinarello/mtb/1 Pinarello - Dogma XC 9.9.jpg'), 'rb'),
    tipo:"mtb",
    color:"Fibra de carbono",
    material:"Carbon Fiber",
    sizes:"S,M,L,XL",
    precio:""
  },
  {
    marca:"Pinarello",
    modelo:"Dogma XC 7.7",
    imagen:File.open(Rails.root.join('db/data/pinarello/mtb/2 Pinarello - Dogma XC 7.7.jpg'), 'rb'),
    tipo:"mtb",
    color:"Fibra de carbono",
    material:"Carbon Fiber",
    sizes:"S,M,L,XL",
    precio:""
  },
  {
    marca:"Pinarello",
    modelo:"Dogma XM 9.9",
    imagen:File.open(Rails.root.join('db/data/pinarello/mtb/3 dogma-xm-9.9.jpg'), 'rb'),
    tipo:"mtb",
    color:"Fibra de carbono",
    material:"Carbon Fiber",
    sizes:"S,M,L,XL",
    precio:""
  },
  {
    marca:"Pinarello",
    modelo:"DOGMA F8 Rhino Paris",
    imagen:File.open(Rails.root.join('db/data/pinarello/ruta/1 DOGMA F8 - TDF2015! - Carbon T11001K - 869 Rhino Paris.jpg'), 'rb'),
    tipo:"ruta",
    color:"Rhino Yellow",
    material:"Carbon T11001K",
    sizes:"",
    precio:""
  },
  {
    marca:"Pinarello",
    modelo:"DOGMA F8",
    imagen:File.open(Rails.root.join('db/data/pinarello/ruta/2 DOGMA F8 - Carbon T11001K - 673 Carbon White Red.jpg'), 'rb'),
    tipo:"ruta",
    color:"Carbon White Red",
    material:"Carbon T11001K",
    sizes:"S,M,L,XL",
    precio:""
  },
  {
    marca:"Pinarello",
    modelo:"DOGMA F8W",
    imagen:File.open(Rails.root.join('db/data/pinarello/ruta/3 DOGMA F8W - Carbon T11001K - 896 Red.jpg'), 'rb'),
    tipo:"ruta",
    color:"Red",
    material:"Carbon T11001K",
    sizes:"S,M,L,XL",
    precio:""
  },
  {
    marca:"Pinarello",
    modelo:"DOGMA K8-S",
    imagen:File.open(Rails.root.join('db/data/pinarello/ruta/4 DOGMA K8-S - T1100 1K - 689 Carbon Red.jpg'), 'rb'),
    tipo:"ruta",
    color:"Carbon Red",
    material:"Carbon T1100 1K",
    sizes:"S,M,L,XL",
    precio:""
  },
  {
    marca:"Pinarello",
    modelo:"GAN RS",
    imagen:File.open(Rails.root.join('db/data/pinarello/ruta/5 GAN RS - Carbon T900 - 244 - LA ROSSA.jpg'), 'rb'),
    tipo:"ruta",
    color:"LA ROSSA",
    material:"Carbon T900",
    sizes:"S,M,L,XL",
    precio:""
  },
  {
    marca:"Pinarello",
    modelo:"PRINCE 60.3",
    imagen:File.open(Rails.root.join('db/data/pinarello/ruta/6 PRINCE 60.3 - Carbon Torayca 60HM3K - 256.jpg'), 'rb'),
    tipo:"ruta",
    color:"Black Yellow",
    material:"Carbon Torayca 60HM3K",
    sizes:"S,M,L,XL",
    precio:""
  },
  {
    marca:"Pinarello",
    modelo:"ROKH",
    imagen:File.open(Rails.root.join('db/data/pinarello/ruta/10 ROKH - Carbon Torayca 30HM12K - 412.jpg'), 'rb'),
    tipo:"ruta",
    color:"Black",
    material:"Carbon Torayca",
    sizes:"S,M,L,XL",
    precio:""
  },
  {
    marca:"Pinarello",
    modelo:"GAN S",
    imagen:File.open(Rails.root.join('db/data/pinarello/ruta/7 GAN S - Carbon T700 - 239.jpg'), 'rb'),
    tipo:"ruta",
    color:"Black Red",
    material:"Carbon T700",
    sizes:"S,M,L,XL",
    precio:""
  },
  {
    marca:"Pinarello",
    modelo:"GAN",
    imagen:File.open(Rails.root.join('db/data/pinarello/ruta/8 GAN - Carbon T600 - 251.jpg'), 'rb'),
    tipo:"ruta",
    color:"Black",
    material:"Carbon T600",
    sizes:"S,M,L,XL",
    precio:""
  },
  {
    marca:"Pinarello",
    modelo:"MERCURIO T2 HYDRO",
    imagen:File.open(Rails.root.join('db/data/pinarello/ruta/13 MERCURIO T2 HYDRO - Carbon 24HMUD - 599.jpg'), 'rb'),
    tipo:"ruta",
    color:"Yellow Black",
    material:"Carbon 24HMUD",
    sizes:"S,M,L,XL",
    precio:""
  },
  {
    marca:"Pinarello",
    modelo:"NEOR",
    imagen:File.open(Rails.root.join('db/data/pinarello/ruta/14 NEOR - ALLOY 6061T6 - 648.jpg'), 'rb'),
    tipo:"ruta",
    color:"White",
    material:"ALLOY 6061T6 - 648",
    sizes:"S,M,L,XL",
    precio:""
  },
  {
    marca:"Pinarello",
    modelo:"TRIONFO ALLOY",
    imagen:File.open(Rails.root.join('db/data/pinarello/ruta/15 TRIONFO ALLOY - ALLOY 6061T6 - 938 Black Yellow Reflex.jpg'), 'rb'),
    tipo:"ruta",
    color:"Black Yellow Reflex",
    material:"LLOY 6061T6 - 938",
    sizes:"S,M,L,XL",
    precio:""
  },
  {
    marca:"Pinarello",
    modelo:"PRIMA",
    imagen:File.open(Rails.root.join('db/data/pinarello/ruta/16 PRIMA - Alloy 6061 T6 - 504.jpg'), 'rb'),
    tipo:"ruta",
    color:"White",
    material:"Alloy 6061 T6 - 504",
    sizes:"S,M,L,XL",
    precio:""
  },

  {
    marca:"Pinarello",
    modelo:"SPEEDY T6",
    imagen:File.open(Rails.root.join('db/data/pinarello/ruta/17 SPEEDY T6 - Ultralight Alloy 6061 T6 - 792 Black.jpg'), 'rb'),
    tipo:"ruta",
    color:"Black",
    material:"Ultralight Alloy",
    sizes:"S,M,L,XL",
    precio:""
  },
  {
    marca:"Pinarello",
    modelo:"GAN RS PINKY",
    imagen:File.open(Rails.root.join('db/data/pinarello/ruta/18 GAN RS - Carbon T900 - 247 - PINKY.jpg'), 'rb'),
    tipo:"ruta",
    color:"Black Pink",
    material:"Carbon T900",
    sizes:"S,M,L,XL",
    precio:""
  },
  {
    marca:"Pinarello",
    modelo:"PRINCE 60.3",
    imagen:File.open(Rails.root.join('db/data/pinarello/ruta/19 PRINCE 60.3 - Carbon Torayca 60HM3K - 258.jpg'), 'rb'),
    tipo:"ruta",
    color:"Black pink",
    material:"Carbon Torayca 60HM3K",
    sizes:"S,M,L,XL",
    precio:""
  },
  {
    marca:"Pinarello",
    modelo:"GAN S",
    imagen:File.open(Rails.root.join('db/data/pinarello/ruta/20 GAN S - Carbon T700 - 240.jpg'), 'rb'),
    tipo:"ruta",
    color:"Black Pink",
    material:"Carbon T700",
    sizes:"S,M,L,XL",
    precio:""
  },
  {
    marca:"Pinarello",
    modelo:"GRAAL Triathlon",
    imagen:File.open(Rails.root.join('db/data/pinarello/tri/1 Pinarello - GRAAL Triathlon.jpg'), 'rb'),
    tipo:"triatlon",
    color:"Nero Red",
    material:"Aerospace Carbon Torayca",
    sizes:"S,M,L,XL",
    precio:""
  },
  {
    marca:"Pinarello",
    modelo:"IL BOLIDE",
    imagen:File.open(Rails.root.join('db/data/pinarello/tri/2 Pinarello - IL BOLIDE.jpg'), 'rb'),
    tipo:"triatlon",
    color:"Gold",
    material:"Aerospace Carbon 65HM1K Torayca",
    sizes:"S,M,L,XL",
    precio:""
  },
  {
    marca:"QuintanaRoo",
    modelo:"PRsix Black",
    imagen:File.open(Rails.root.join('db/data/QuintanaRoo/1 QuintanaRoo - PRsix Black.jpg'), 'rb'),
    tipo:"triatlon",
    color:"Black",
    material:"Superlight Carbon Fiber",
    sizes:"48,50,52,54,56,58.5",
    precio:""
  },
  {
    marca:"QuintanaRoo",
    modelo:"PRfive",
    imagen:File.open(Rails.root.join('db/data/QuintanaRoo/2 QuintanaRoo - PRfive.jpg'), 'rb'),
    tipo:"triatlon",
    color:"Red",
    material:"Superlight Carbon Fiber",
    sizes:"48,50,52,54,56,58.5",
    precio:""
  },
  {
    marca:"QuintanaRoo",
    modelo:"PRsix USA Limited Edition",
    imagen:File.open(Rails.root.join('db/data/QuintanaRoo/3 QuintanaRoo - PRsix USA Limited Edition Bike.jpg'), 'rb'),
    tipo:"triatlon",
    color:"USA Limited Edition",
    material:"Superlight Carbon Fiber",
    sizes:"48,50,52,54,56,58.5",
    precio:""
  },
  {
    marca:"QuintanaRoo",
    modelo:"CD01 Di2",
    imagen:File.open(Rails.root.join('db/data/QuintanaRoo/4 QuintanaRoo - CD01 Di2.jpg'), 'rb'),
    tipo:"triatlon",
    color:"Black",
    material:"Superlight Carbon Fiber",
    sizes:"48,50,52,54,56,58.5",
    precio:""
  },
  {
    marca:"QuintanaRoo",
    modelo:"CD0.1 105",
    imagen:File.open(Rails.root.join('db/data/QuintanaRoo/5 QuintanaRoo - CD0.1 105.jpg'), 'rb'),
    tipo:"triatlon",
    color:"Black White",
    material:"Superlight Carbon Fiber",
    sizes:"48,50,52,54,56,58.5",
    precio:""
  },
  {
    marca:"QuintanaRoo",
    modelo:"CD0.1 Digital",
    imagen:File.open(Rails.root.join('db/data/QuintanaRoo/6 QuintanaRoo - CD0.1 Digital.jpg'), 'rb'),
    tipo:"triatlon",
    color:"Black Pink",
    material:"Superlight Carbon Fiber",
    sizes:"48,50,52,54,56,58.5",
    precio:""
  },
  {
    marca:"QuintanaRoo",
    modelo:"Kilo",
    imagen:File.open(Rails.root.join('db/data/QuintanaRoo/7 Kilo.jpg'), 'rb'),
    tipo:"triatlon",
    color:"Black",
    material:"Superlight Carbon Fiber",
    sizes:"48,50,52,54,56,58.5",
    precio:""
  },
  {
    marca:"QuintanaRoo",
    modelo:"Dulce",
    imagen:File.open(Rails.root.join('db/data/QuintanaRoo/8 Dulce.jpg'), 'rb'),
    tipo:"triatlon",
    color:"Pink Black",
    material:"Superlight Carbon Fiber",
    sizes:"48,50,52,54,56,58.5",
    precio:""
  },
  {
    marca:"QuintanaRoo",
    modelo:"Lucero",
    imagen:File.open(Rails.root.join('db/data/QuintanaRoo/9 Lucero.jpg'), 'rb'),
    tipo:"triatlon",
    color:"Black Red",
    material:"Superlight Carbon Fiber",
    sizes:"48,50,52,54,56,58.5",
    precio:""
  },
])
